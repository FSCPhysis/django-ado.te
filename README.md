# Django - ADO.TE
Aplicação web completa desenvolvida para viabilizar adoção de Pets, contendo área do usuario, cadastro de pets, processamento de pedidos e busca por região e raça.
Projeto desenvolvido durante a PyStack Week, da Pythonando, com maior foco no back-end.

## Tecnologias Utilizadas
* Python
* Django
* SQLite
* HTML5
* CSS3
* Bootstrap5

## Funcionalidades
- Cadastro de usuarios e sistema de autenticação.
- Sistema de cadastro de Pets para adoção.
- Lista de pets disponiveis e seus detalhes.
- Sistema para solicitação e processamento de adoção.
- Dashboard de adoções realizadas com sucesso.

## Templates Principais
![login](https://gitlab.com/FSCPhysis/django-ado.te/-/raw/main/Prints/login.png)
![Cadastro](https://gitlab.com/FSCPhysis/django-ado.te/-/raw/main/Prints/cadastro.png)
![Cadastrar novo Pet](https://gitlab.com/FSCPhysis/django-ado.te/-/raw/main/Prints//divulgar-novo_pet.png)
![Seus Pets](https://gitlab.com/FSCPhysis/django-ado.te/-/raw/main/Prints//divulgar-seus_pets.png)
![Busca de Pets para adoção](https://gitlab.com/FSCPhysis/django-ado.te/-/raw/main/Prints/adotar.png)
![Detalhes do Pet](https://gitlab.com/FSCPhysis/django-ado.te/-/raw/main/Prints/divulgar-ver_pet.png)
![Pedidos de adoção](https://gitlab.com/FSCPhysis/django-ado.te/-/raw/main/Prints/divulgar-ver_pedidos_adoco.png)

## Instalação

Os modulos necessários estão listados no arquivo requirements.txt, para intala-los usando o pip:
```python
pip install -r requirements.txt
```
Para iniciar a aplicação:
```python
python manage.py runserver
```

## Licença

**MIT**
