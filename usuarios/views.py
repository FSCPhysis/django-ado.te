from django.shortcuts import render
from django.shortcuts import redirect
from django.http import HttpResponse
from django.contrib.auth.models import User
from django.contrib import messages
from django.contrib.messages import constants
from django.contrib.auth import authenticate, login, logout


def cadastro(request):
    if request.user.is_authenticated:
        return redirect('/divulgar/novo_pet')
    if request.method =="GET":
        return render(request, 'cadastro.html')
    elif request.method == "POST":
    # Obtendo dados do formulário.
        nome = request.POST.get('nome')
        email = request.POST.get('email')
        senha = request.POST.get('senha')
        confirmar_senha = request.POST.get('confirmar_senha')
        
        ## Verificando se o formulário foi preenchido corretamente.
        # Verificando se os campos estão todos preenchidos:
        if len(nome.strip()) == 0 or len(email.strip()) == 0 or len(senha.strip()) == 0 or len(confirmar_senha.strip()) == 0:
            # Mensagem de Erro para compos não preenchidos
            messages.add_message(request, constants.ERROR, 'Preencha todos os campos')
            return render(request, 'cadastro.html')
        
        # Verificando se a senha e confirmar_senha são iguais:
        if senha != confirmar_senha:
            # Mensagem de de erro para senhas diferentes
            messages.add_message(request, constants.ERROR, 'Digite duas senhas iguais')
            return render(request, 'cadastro.html')
        
        try:
            user = User.objects.create_user(
                    username = nome,
                    email = email,
                    password = senha
            )
            # Mensagem de sucesso
            messages.add_message(request, constants.SUCCESS, 'Usuário criado com sucesso!')
            return render(request, 'cadastro.html')
        except:
            # Mensagem de erro
            messages.add_message(request, constants.ERROR, 'Erro interno do sistema, por gentileza, tente mais tarde!')
            return render(request, 'cadastro.html')
            
def logar(request):
    if request.user.is_authenticated:
        return redirect('/divulgar/novo_pet')
    if request.method == "GET":
        return render(request, 'login.html')
    elif request.method == "POST":
        nome = request.POST.get('nome')
        senha = request.POST.get('senha')
        
        # Autenticação de usuario
        user = authenticate(username=nome, password=senha)
        if user is not None:
            login(request, user)
            return redirect('/divulgar/novo_pet')
        # Logar usuario
        else:
            messages.add_message(request, constants.ERROR, "Usuario ou senha incorretos!")
            return render(request, "login.html")
            
def sair(request):
    logout(request)
    return redirect('/auth/login')